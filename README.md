# Aplikasi Nasabah #

Menyediakan REST API untuk data nasabah

## Deploy Single App ##

1. Buat aplikasi di Heroku, misalnya nama aplikasinya `tm2018-nasabah`

        heroku apps:create tm2018-nasabah

2. Enable Dyno Info melalui CLI

        heroku labs:enable runtime-dyno-metadata

3. Set konfigurasi profile Spring

        heroku config:set SPRING_PROFILES_ACTIVE=heroku

4. Buat database postgresql yang gratisan

        heroku addons:create heroku-postgresql:hobby-dev

5. Tambahkan remote git Heroku

        git remote add heroku https://git.heroku.com/tm2018-config.git

6. Push ke Heroku

        git push heroku master

## Replikasi (mendeploy beberapa instance) ##

1. Buat aplikasi di Heroku

        heroku apps:create tm2018-nasabah-1
        heroku apps:create tm2018-nasabah-2

2. Enable Dyno Info, sebutkan aplikasinya dengan opsi `-a`

        heroku labs:enable runtime-dyno-metadata -a tm2018-nasabah-1
        heroku labs:enable runtime-dyno-metadata -a tm2018-nasabah-2

3. Set konfigurasi profile Spring

        heroku config:set SPRING_PROFILES_ACTIVE=heroku -a tm2018-nasabah-1
        heroku config:set SPRING_PROFILES_ACTIVE=heroku -a tm2018-nasabah-2

4. Aplikasi replika ini akan sharing database dengan aplikasi pertama. Cari tahu dulu nama database di aplikasi pertama

        heroku addons -a tm2018-nasabah
    
    Outputnya seperti ini
    
        Add-on                                            Plan       Price  State  
        ────────────────────────────────────────────────  ─────────  ─────  ───────
        heroku-postgresql (postgresql-trapezoidal-49829)  hobby-dev  free   created
         └─ as DATABASE
        
        The table above shows add-ons and the attachments to the current app (tm2018-nasabah) or other apps.

5. Sambungkan (attach) instance database `postgresql-trapezoidal-49829` ke aplikasi `tm2018-nasabah-1` dan `tm2018-nasabah-2`

        heroku addons:attach postgresql-trapezoidal-49829 -a tm2018-nasabah-1
        heroku addons:attach postgresql-trapezoidal-49829 -a tm2018-nasabah-2

## Replikasi di Local Laptop ##

1. Buat beberapa profile, di dalamnya dibedakan portnya. 

    * Profile `20001` ditulis di file `application-20001.properties` sebagai berikut
    
        spring.profiles.include=default
        server.port=20001
     
    * Profile `20002` ditulis di file `application-20002.properties` sebagai berikut
    
        spring.profiles.include=default
        server.port=20002

2. Jalankan aplikasi dengan profile berbeda di dua console

        mvn spring-boot:run -Dspring.profiles.active=20001
        mvn spring-boot:run -Dspring.profiles.active=20002