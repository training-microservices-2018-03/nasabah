package com.artivisi.training.microservice201803.nasabah.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@RestController
public class Info {

    @GetMapping("/api/info")
    public Map<String, String> hostInfo(HttpServletRequest request){
        Map<String, String> info = new HashMap<>();
        info.put("hostname", request.getLocalName());
        info.put("ip", request.getLocalAddr());
        info.put("port", String.valueOf(request.getLocalPort()));
        return info;
    }
}
