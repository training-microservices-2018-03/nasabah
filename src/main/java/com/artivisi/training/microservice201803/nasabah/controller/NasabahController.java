package com.artivisi.training.microservice201803.nasabah.controller;

import com.artivisi.training.microservice201803.nasabah.dao.NasabahDao;
import com.artivisi.training.microservice201803.nasabah.entity.Nasabah;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/nasabah")
public class NasabahController {
    private static final Logger LOGGER = LoggerFactory.getLogger(NasabahController.class);

    @Autowired private NasabahDao nasabahDao;

    @GetMapping("/")
    public Iterable<Nasabah> semuaNasabah(){
        LOGGER.info("Menyediakan data nasabah");
        return nasabahDao.findAll();
    }
}
