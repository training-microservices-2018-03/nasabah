package com.artivisi.training.microservice201803.nasabah.entity;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Entity @Data
public class Nasabah {

    @Id @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @NotBlank
    private String nomor;

    @NotBlank
    private String nama;

    @NotBlank @Email
    private String email;

    @NotBlank
    private String noHp;
}
