package com.artivisi.training.microservice201803.nasabah.dao;

import com.artivisi.training.microservice201803.nasabah.entity.Nasabah;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface NasabahDao extends PagingAndSortingRepository<Nasabah, String> {
}
