package com.artivisi.training.microservice201803.nasabah;

import com.artivisi.training.microservice201803.nasabah.dao.NasabahDao;
import com.artivisi.training.microservice201803.nasabah.entity.Nasabah;
import com.github.javafaker.Faker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NasabahApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(NasabahApplication.class, args);
	}

	private static final Logger LOGGER = LoggerFactory.getLogger(NasabahApplication.class);
	@Autowired private NasabahDao nasabahDao;

	@Override
	public void run(String... args) throws Exception {
		//generateSampleData();
	}

	private void generateSampleData() {
		nasabahDao.deleteAll();
		int jumlahData = 100;

		Faker faker = new Faker();

		for (int i = 0; i < jumlahData; i++) {
			String namaDepan = faker.name().firstName();
			String namaBelakang = faker.name().lastName();
			String domain = faker.internet().domainName();
			String noHp = faker.phoneNumber().cellPhone();

			Nasabah n = new Nasabah();
			n.setNama(namaDepan + " " +namaBelakang);
			n.setEmail(namaDepan.toLowerCase()+"."+namaBelakang.toLowerCase()+"@"+domain);
			n.setNoHp(noHp);
			n.setNomor(faker.number().digits(10));
			nasabahDao.save(n);

			LOGGER.info("Insert nasabah {}", n);
		}
	}
}
